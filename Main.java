import com.Solution;

import static java.lang.Math.round;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        //int [] arr = new int []{10,3,8,4};
        int [] arr=solution.generateArray(1000000);
        System.out.println("max avg subarray = "+solution.findMaxAverage(arr,4));
    }
}
